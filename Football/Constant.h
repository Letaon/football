//
//  Constant.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Football_Constant_h
#define Football_Constant_h

//названия нотификаций
#define NotificationPlayerDidSelect       @"com.Football.player-Select "
#define NotificationPlayerWayToRun   @"com.Football.player-wayToRun"
#define NotificationBallCached       @"com.Football.ball-cached"

#endif
