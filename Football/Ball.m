//
//  Ball.m
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Ball.h"
#import "Constant.h"



@implementation Ball
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        [self addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ball.png"]]];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ballCached:) name:NotificationBallCached object:nil];
        [[self.subviews objectAtIndex:0] setFrame:CGRectMake(12.5, 12.5, 15, 15)];
        origin = self.frame.origin;
        //self.backgroundColor = [UIColor greenColor];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch;
    touch = [touches anyObject];
    CGPoint coords = [touch locationInView:self.superview];
    //NSLog(@"%@",NSStringFromCGPoint(coords));
    
    speed.x = (self.frame.origin.x - coords.x)/4;
    speed.y = (self.frame.origin.y - coords.y)/4;
    
    if(sqrtf(speed.x * speed.x + speed.y * speed.y) > 30)
    {
        NSLog(@"%f",sqrtf(speed.x * speed.x + speed.y * speed.y));
    }
    
    NSLog(@"%@",NSStringFromCGPoint(speed));
    
    

    if([self.delegate respondsToSelector:@selector(ballKicked:WithSpeed:)])
        [self.delegate ballKicked:self WithSpeed:speed];
    //[self kick];
}


-(void)kick
{

   shotTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(fly:) userInfo:nil repeats:YES];
}


-(void)fly:(id)sender
{
    
    
    origin.x += speed.x;
    origin.y += speed.y;
   speed.x *=0.95;
    speed.y *= 0.95;
    
    
    
    if(origin.x > 753 || origin.x < 0)
    {
        if(origin.x > 753)
            origin.x = 753;
        if(origin.x < 0)
            origin.x = 0;
        speed.x*=-1;
    }
    if(origin.y > 1004 || origin.y < 20)
    {
        
        if(origin.x >288 && origin.x < 480)
        {
            if([self.delegate respondsToSelector:@selector(goal:toTeam:)])
                [self.delegate goal:self toTeam:(origin.y < 20)];
            //[shotTimer invalidate];
            //shotTimer = nil;
            //return;
        }
        else
            
        {
            if(origin.y > 1004)
                origin.y = 1004;
            if( origin.y < 20)
                origin.y = 20;
            speed.y = speed.y * -1;
        }
    }
    
    
    self.frame = CGRectMake(origin.x, origin.y, 40, 40);

    if(ABS(speed.x) <1 && ABS(speed.y) < 1)
    {
        [shotTimer invalidate];
        shotTimer = nil;
    }
}

-(void)ballCached:(id)sender
{
    //поймали мяч
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
