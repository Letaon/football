//
//  GmaeFieldViewController.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamMannager.h"
#import "Constant.h"
#import "Ball.h"
#import "Player.h"
#import "Vector.h"

@interface GameFieldViewController : UIViewController<BallDelegate>
{
    TeamMannager *redTeam;
    TeamMannager *blueTeam;
    UILabel *goalLabel;
    Ball *ball;
    
    IBOutlet UIImageView *backgroundImage;
    
    NSMutableArray *currentTouchCoords;
    
    Player *selectedPlayer;
     NSMutableArray *coords;
     IBOutlet Vector *vectorView;
    
    BOOL currentTeam;
}

-(void)initGame;
-(IBAction)nextStep:(id)sender;
-(void)restartGame;
@end
