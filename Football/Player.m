//
//  Player.m
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"
#import "Constant.h"

@implementation Player
@synthesize wayToRun = _wayToRun;

@synthesize isWithBall = _isWithBall;
@synthesize teamA = _teamA;
@synthesize nextLocation = _nextLocation;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.nextLocation = self.frame.origin;
        // Initialization code
    }
    return self;
}

-(id)initWithTeam:(BOOL)teamFlag frame:(CGRect)frame
{
    self.teamA = teamFlag;
    //UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"player.png"]];
    //[self addSubview:img];
    return [self initWithFrame:frame];
}


-(void)runInWay:(NSArray *)way
{
    if(!_wayToRun)
        _wayToRun = [[NSMutableArray alloc] init];
    
    [_wayToRun removeAllObjects];
    [_wayToRun addObjectsFromArray:way];
}


-(void)react
{
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[[NSNotificationCenter defaultCenter] postNotificationName:NotificationPlayerDidSelect object:self];
    
    if(!_wayToRun)
        _wayToRun = [[NSMutableArray alloc] init];
    [_wayToRun removeAllObjects];
    
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self.superview];

    
    NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:p.x],@"x",[NSNumber numberWithFloat:p.y - 20],@"y", nil];

    
    [self.wayToRun addObject:d];
    lastTouch = p;
}
- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [touches anyObject];
      CGPoint p = [touch locationInView:self.superview];
    
    float d = sqrt((lastTouch.x - p.x) * (lastTouch.x - p.x) + (lastTouch.y - p.y) * (lastTouch.y - p.y));
    
    
    if(d > 100)
    {
        if(p.x - lastTouch.x > 0)
            p.x = lastTouch.x + (p.x - lastTouch.x) / (d / 100) ;
        else
            p.x = lastTouch.x - (lastTouch.x - p.x) / (d / 100) ;
        
        if(p.y - lastTouch.y > 0)
            p.y = lastTouch.y + (p.y - lastTouch.y) / (d / 100) ;
        else
            p.y = lastTouch.y - (lastTouch.y - p.y) / (d / 100) ;
        
        
        
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:p.x],@"x",[NSNumber numberWithFloat:p.y - 20],@"y", nil];
        [_wayToRun addObject:d];
        lastTouch = p;
    }
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"%@",_wayToRun);
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationPlayerWayToRun object:self];
}

-(void)moveToPoint:(CGPoint)newLocation
{
    
    [UIView animateWithDuration:1 animations:^{
        self.frame = CGRectMake(newLocation.x, newLocation.y,self.frame.size.width,self.frame.size.width);} 
                     completion:^(BOOL finished) {
                         self.alpha = 1.0f;
                         [UIView animateWithDuration:0.5 animations:^{
                             self.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                         }completion:^(BOOL finished) {self.nextLocation = self.frame.origin;}];
                     }];
    
    
}

-(void)run
{
    for(NSDictionary *d in self.wayToRun)
    {
        CGPoint pointToRun;
        pointToRun.x = [[d valueForKey:@"x"] floatValue];
        pointToRun.y = [[d valueForKey:@"y"] floatValue];
        [self moveToPoint:pointToRun];
        
    }
}

@end
