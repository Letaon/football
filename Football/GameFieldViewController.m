//
//  GmaeFieldViewController.m
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameFieldViewController.h"

@implementation GameFieldViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerDidClicked:) name:NotificationPlayerDidSelect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerWaySeted:) name:NotificationPlayerWayToRun object:nil];

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initGame];
    currentTeam = 0;
    // Do any additional setup after loading the view from its nib.
    
   
    
}

- (void)viewDidUnload
{
    backgroundImage = nil;
    vectorView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)initGame
{
    coords = [[NSMutableArray alloc] init];
    redTeam = [[TeamMannager alloc]initTeamWithColor:[UIColor redColor] teamNumber:0];
    
    
    ball =[[Ball alloc] initWithFrame:CGRectMake(768/2-20, 1044/2-20, 40, 40)];
    
    ball.delegate = self;
    
    [self.view addSubview:ball];

    for(Player *pl in redTeam.team)
    {
        [self.view addSubview:pl];
        [self.view bringSubviewToFront:pl];
    }
    blueTeam = [[TeamMannager alloc]initTeamWithColor:[UIColor blueColor] teamNumber:1];
    for(Player *pl in blueTeam.team)
    {
        [self.view addSubview:pl];
         [self.view bringSubviewToFront:pl];
    }
    [self.view bringSubviewToFront:ball];
    
}

-(void)restartGame
{
    coords = nil;
    for(Player *p in redTeam.team)
    {
        [p removeFromSuperview];
    }
    for(Player *p in blueTeam.team)
    {
        [p removeFromSuperview];
    }
    
    [ball removeFromSuperview];
    [goalLabel removeFromSuperview];
    ball = nil;
    blueTeam = nil;
    redTeam = nil;
    
    [self initGame];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  /*  
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.view];
    if(selectedPlayer)
    {
        NSDictionary *point = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:location.x],@"x",[NSNumber numberWithFloat:location.y],@"y", nil];
        [coords addObject:point];
    }

    selectedPlayer.nextLocation = location;
    selectedPlayer = nil;
   */
}
- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
   /* if([coords count])
    {
        
        Vector *line = [[Vector alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
        line.lineCoords = coords;
        [vectorView addSubview:line];
    }
    */
}

-(void)playerWaySeted:(NSNotification*)notification
{
    Vector *line = [[Vector alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
    selectedPlayer = notification.object;

    for(Vector *l in vectorView.subviews)
    {
        if(l.player == selectedPlayer)
            [l removeFromSuperview];
    }
    
    
    line.player = selectedPlayer;
    
    line.lineCoords = selectedPlayer.wayToRun;
    
    [vectorView addSubview:line];
}


-(void)playerDidClicked:(NSNotification*)notification
{
    //очишаем для того чтоб рисовался только 1 вектор за ход
    if(!selectedPlayer)
    {
        if([coords count])
        {
            [coords removeAllObjects];
            /* NSArray *arr = [NSArray arrayWithArray:[vectorView subviews]];
             for(Vector *vect in arr)
             {
             [vect removeFromSuperview];
             }*/
        }
        selectedPlayer = notification.object;
        if(selectedPlayer.teamA == currentTeam)
        {
            [UIView animateWithDuration:0.5 animations:^{
                selectedPlayer.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
            }];
            NSDictionary *point = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:selectedPlayer.frame.origin.x],@"x",[NSNumber numberWithFloat:selectedPlayer.frame.origin.y],@"y", nil];
            [coords addObject:point];
        }
        else 
        {
            selectedPlayer = nil;
        }
    }
}

-(IBAction)nextStep:(id)sender
{
    //убираем все вектора с вью
    //  if([coords count])
    // {
    //     [coords removeAllObjects];
    NSArray *arr = [NSArray arrayWithArray:[vectorView subviews]];
    for(Vector *vect in arr)
    {
        [vectorView removeFromSuperview];
    }
    //}
    
    //__block void (^move)(void);
    // __block void (^move2)(void);
    // move=^{
    for(Player *pl in redTeam.team)
    {
        [pl run];
        //[pl moveToPoint:pl.nextLocation];
    }
    // };
    // move2=^
    // {
    for(Player *pl in blueTeam.team)
    {
        [pl run];
        //[pl moveToPoint:pl.nextLocation];
    }
    //};
    currentTeam = !currentTeam;
 
}
#pragma mark BallDelegate
-(void)ballKicked:(Ball *)aBall WithSpeed:(CGPoint)speedVector
{
    [ball kick];
}

-(void)goal:(Ball *)aBall toTeam:(BOOL)team
{
    
    if(!goalLabel)
        goalLabel = [[UILabel alloc] initWithFrame:CGRectMake(300, 480, 166, 50)];
    [goalLabel setText:@"Goal!!!"];
    goalLabel.alpha = 0.0;
    [self.view addSubview:goalLabel];
    [self.view bringSubviewToFront:goalLabel];
    if(team)
        goalLabel.backgroundColor = [UIColor redColor];
    else 
        goalLabel.backgroundColor = [UIColor blueColor];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationRepeatCount:1];
    [self performSelector:@selector(restartGame) withObject:nil afterDelay:1.0];
    goalLabel.alpha = 0.0;
    goalLabel.alpha = 1.0;
    goalLabel.alpha = 0.0;
    goalLabel.alpha = 1.0;
    goalLabel.alpha = 0.0;
    //[self restartGame];
    
    [UIView commitAnimations];
    }
@end
