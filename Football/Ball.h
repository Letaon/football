//
//  Ball.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Ball;
@protocol BallDelegate <NSObject>

-(void)ballKicked:(Ball*)aBall  WithSpeed:(CGPoint)speedVector;
-(void)goal:(Ball*)aBall toTeam:(BOOL)team;

@end



@interface Ball : UIView
{
    //CGPoint acceleration;
    CGPoint speed;
    CGPoint origin;
    
    
    NSTimer *shotTimer;
}

-(void)kick;
@property(nonatomic,assign)id <BallDelegate>delegate;
@end
