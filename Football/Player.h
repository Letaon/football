//
//  Player.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Player : UIView
{
    BOOL _teamA;
    BOOL _isWithBall;
    
    //CGPoint coordToGo;
    CGPoint lastTouch;
    NSMutableArray *_wayToRun;   
    
}

@property(nonatomic) BOOL teamA;
@property(nonatomic) BOOL isWithBall;
@property(nonatomic) CGPoint nextLocation;
@property(nonatomic) NSMutableArray *wayToRun;

-(void)runInWay:(NSArray*)way;
-(id)initWithTeam:(BOOL)teamFlag frame:(CGRect)frame;

//реагирование на действия противника, или полет мяча
-(void)react;
-(void)moveToPoint:(CGPoint)newLocation;
-(void)run;
@end
