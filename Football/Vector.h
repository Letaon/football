//
//  Vector.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Player.h"

@interface Vector : UIView
{
    NSArray *_coords;
    NSMutableArray *_figureCoords;
    float randomShift;
    
}
@property(nonatomic,retain) NSArray  *lineCoords;
@property(nonatomic) Player *player;
@end
