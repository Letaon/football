//
//  Vector.m
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Vector.h"

@implementation Vector

@synthesize lineCoords = _coords;
@synthesize player;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}



- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGFloat red[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    CGContextSetStrokeColor(c, red);
    CGContextBeginPath(c);
    CGContextSetLineWidth(c, 1.0);
    
    
    CGContextMoveToPoint(c, [[[_coords objectAtIndex:0] valueForKey:@"x"] floatValue], [[[_coords objectAtIndex:0] valueForKey:@"y"] floatValue]);
    
    if([_coords count] >1)
        for(int i = 0; i<[_coords count]; i++)
        {
            
            CGContextAddLineToPoint(c, [[[_coords objectAtIndex:i] valueForKey:@"x"] floatValue],[[[_coords objectAtIndex:i] valueForKey:@"y"] floatValue]);
        }
       CGContextStrokePath(c);
}


@end
