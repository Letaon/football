//
//  CammandMannager.m
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TeamMannager.h"

@implementation TeamMannager

@synthesize team = _team;



-(id)initTeamWithColor:(UIColor *)teamColor teamNumber:(BOOL)A
{
    self =  self = [[TeamMannager alloc]init];
    if(self)
    {
        teamA = A;
        if(!color)
            color = [[UIColor alloc] init];
        color = teamColor;
        
        self.team = [[NSMutableArray alloc]initWithArray:[self setTeamPlayers]];

        
    }
    return self;
}

-(NSMutableArray*)setTeamPlayers
{   
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    float basicCoords[5][2];
    basicCoords[0][0] = 192;
    basicCoords[0][1] = 170;
    basicCoords[1][0] = 384;
    basicCoords[1][1] = 170;
    basicCoords[2][0] = 576;
    basicCoords[2][1] = 170;
    basicCoords[3][0] = 256;
    basicCoords[3][1] = 340;
    basicCoords[4][0] = 512;
    basicCoords[4][1] = 340;
    
    for(int i = 0 ; i < 5 ; i ++)
    {
#warning need correct init metods
       
        CGRect rect;
        if(teamA)
            rect = CGRectMake(basicCoords[i][0], basicCoords[i][1], 20, 20);
        else
            rect = CGRectMake(768 -  basicCoords[i][0], 1004 - basicCoords[i][1], 20, 20);
        
        Player *player = [[Player alloc]initWithTeam:teamA frame:rect];
        player.tag = i;
        player.backgroundColor = color;
        [arr addObject:player];
    }
    return arr;
}

@end
