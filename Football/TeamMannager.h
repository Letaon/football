//
//  CammandMannager.h
//  Football
//
//  Created by Gleb Pavluchenko on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface TeamMannager : NSObject

{
    NSMutableArray *_team;
    BOOL teamA;
    UIColor *color;
    
}

//-(id)initTeam;
-(id)initTeamWithColor:(UIColor*)teamColor teamNumber:(BOOL)A;

-(NSMutableArray*)setTeamPlayers;

@property(nonatomic,retain) NSMutableArray *team;

@end
